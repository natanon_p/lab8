package pizzashop.food;

public interface OrderItem {
	String getSize();
	boolean sizeValid();
}
