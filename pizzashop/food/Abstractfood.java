package pizzashop.food;

public abstract class Abstractfood implements OrderItem{
	protected int size;
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	
	public Abstractfood(int size){
		this.size = size;
	}
	public String getSize(){
		if(sizeValid()) return sizes[size];
		return null;
	}
	public boolean sizeValid(){
		return size >= 0 && size < sizes.length;
	}
}
