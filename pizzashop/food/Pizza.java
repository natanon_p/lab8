package pizzashop.food;
import java.util.*;

import pizzashop.*;
/** 
 * A pizza, of course.
 */
public class Pizza extends Abstractfood{
	public static final double [] prices = { 0.0, 120.0, 200.0, 300.0 };
	/** list of toppings on this pizza */
	private List<Topping> toppings;

	/** A new pizza.
	 * @param size is 1 for small, 2 for medium, 3 for large.
	 */
	public Pizza( int size ) {
		super(size);
		toppings = new ArrayList<Topping>();
	}
	
	public String toString() {
		StringBuffer tops = new StringBuffer();
		if ( toppings.size() == 0 || 
				(toppings.size()==1 && toppings.get(0) == Topping.NONE ) ) ;
		else {
			tops.append(" with ");
			tops.append(toppings.get(0).toString());
			for( int k = 1; k<toppings.size(); k++ ) {
				tops.append(", ");
				tops.append( toppings.get(k).toString() );
			}
		}

		return String.format("%s pizza%s", getSize(), tops.toString() );
	}

	public double getPrice() {
		double price = 0;
		if ( super.sizeValid() ) price = prices[size];
		for( Topping top : toppings) price += top.getPrice();
		return price;
	}

	/** 
	 * add a topping
	 * @param topping is a topping to add to pizza
	 */
	public void add(Topping topping) {
		toppings.add( topping );
	}

	public void remove(Topping topping) {
		toppings.remove(topping);
	}

	public List<Topping> getToppings( ) {
		return toppings;
	}

	public void setToppings( List<Topping> toppings ) {
		this.toppings = toppings;
	}

	public Object clone() {
		Pizza clone = null;
		try {
			clone = (Pizza) super.clone();
			clone.size = this.size;
			clone.toppings = new ArrayList<Topping>();
			for(Topping t : toppings ) clone.toppings.add(t);
		} catch (CloneNotSupportedException e) {
			System.out.println("Pizza.clone: " + e.getMessage() );
		}

		return clone;
	}
}
